package com.rahmanarifofficial.androidapinetworking

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        playground()
        //loadImage()
        //initEvent()
        fetchAllData()
    }

    private fun fetchAllData(){
        ApiClient.apiClient.getMovieNowPlaying(ApiKey.apiKeyTMDB)
            .enqueue(object : Callback<Movie>{
                override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                    val movie = response.body()
                    val listMovie = movie?.results
                    // listMovie masukan kedalam adapter
                    // adapter dihubungkan ke recyclerview
                    Log.e("MANTAP1", listMovie?.get(0).toString())
                    Log.e("MANTAP2", Gson().toJson(listMovie?.get(0)))
                }

                override fun onFailure(call: Call<Movie>, t: Throwable) {
                    Log.e("MANTAP", t.message.toString())
                }

            })
    }

    private fun initEvent(){
        findViewById<Button>(R.id.btnaction).setOnClickListener {
            if (!PermissionUtils.isPermissionGranted(this, Manifest.permission.READ_CONTACTS)) {
                PermissionUtils.requestPermission(
                    this,
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    100
                )
            } else {
                //Jalankan logic
                Toast.makeText(this, "Permission already granted", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(
                this@MainActivity,
                permission
            ) == PackageManager.PERMISSION_DENIED
        ) {

            // Requesting the permission
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(permission), requestCode)
        } else {
            Toast.makeText(this@MainActivity, "Permission already granted", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this@MainActivity, "Contact Permission Granted", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(this@MainActivity, "Contact Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    fun loadImage() {
        Glide.with(this)
            .load("https://i.ibb.co/zJHYGBP/binarlogo.jpg")
            .into(findViewById(R.id.image))
    }

    fun playground() {
        val jsonObject = JSONObject()
        jsonObject.put("name", "Arif")
        jsonObject.put("age", 25)

        val json = "{\"phonetype\":\"N95\",\"cat\":\"WP\"}"
        val jsonObject2 = JSONObject(json)

        Log.e("BinarAcademy5", jsonObject2.getString("phonetype"))

    }
}