package com.rahmanarifofficial.androidapinetworking

data class Movie(
    var page: Int?,
    var results: List<Movies?>?,
    var total_pages: Int?,
    var total_results: Int?
)