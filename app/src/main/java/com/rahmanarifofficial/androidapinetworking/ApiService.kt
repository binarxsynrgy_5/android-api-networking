package com.rahmanarifofficial.androidapinetworking

import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    //movie/now_playing?api_key=apikey
    @GET("movie/now_playing")
    fun getMovieNowPlaying(@Query("api_key") apiKey: String): Call<Movie>

}